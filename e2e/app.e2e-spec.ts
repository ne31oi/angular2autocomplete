import { Angular2autocompletePage } from './app.po';

describe('angular2autocomplete App', () => {
  let page: Angular2autocompletePage;

  beforeEach(() => {
    page = new Angular2autocompletePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
