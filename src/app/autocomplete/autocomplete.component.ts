import {Component, OnInit} from '@angular/core';
import  {AutocompleteService} from './autocomplete.service'
import 'rxjs/add/operator/startWith';


@Component({
  selector: 'autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css'],
  providers: [AutocompleteService]
})


export class AutocompleteComponent implements OnInit {
  private dropdownVisible = false;
  private filteredList: any[] = [];
  private keyword: string = '';
  private maxItems = 5;
  private source: any;
  private visibleItems;
  private arr = Array;
  private valid: boolean = true;


  constructor(private _autocompleteservice: AutocompleteService) {
  }


  showDropdownList(): void {
    if (this.keyword.length > 0) {
      this.dropdownVisible = true;
    }
    this.valid = true;
  }


  hideDropdownList(): void {
    this.dropdownVisible = false;
    if (this.filteredList.length != 1) {
      this.valid = false;
    }
    else {
      if(this.keyword!=this.filteredList[0].City){this.valid = false;}

    }
  }


  filter() {
    if (this.keyword.length > 0) {
      this.filteredList = [];
      for (let item of this.source) {
        let i: string = item.City.toLowerCase();
        if (i.substring(0, this.keyword.length).indexOf(this.keyword.toLowerCase()) != -1) {
          this.filteredList.push(item);
        }
      }
      if (this.filteredList.length > this.maxItems) {
        this.visibleItems = this.maxItems;
      }
      else {
        this.visibleItems = this.filteredList.length;
      }
      this.showDropdownList();
    }
    else {
      this.hideDropdownList();
    }
  }


  select(i) {
    this.keyword = i.City;
    this.valid = true;
    this.filteredList = [];
    this.filter();
    this.hideDropdownList();
  }


  ngOnInit() {
    this._autocompleteservice.getCity().subscribe(city => {
      this.source = city;
    })
  }

}
