import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {AutocompleteComponent} from './autocomplete/autocomplete.component';


@NgModule({
  declarations: [

    AppComponent,
    AutocompleteComponent
  ],
  imports: [
    [CommonModule],
    BrowserModule,
    FormsModule,
    HttpModule,

    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
